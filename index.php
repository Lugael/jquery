<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <nav>
        <a href="index.php">Home</a>
        <a href="index.php?page=cadAdm">Cadastro administradora</a>
        <a href="index.php?page=administradoras">administradora</a>
        <a href="index.php?page=condominios">Condominios</a>
        <a href="index.php?page=blocos">blocos</a>
        <a href="index.php?page=unidade">unidades</a>
        <a href="index.php?page=moradores">moradores</a>
    </nav>
    <main class="container">
        <?
        switch ($_GET['page']) {
            case '':
            case 'dashbord':
                require "views/dashboard.php";
                break;
            default:
                require "views/".$_GET['page'].'.php';
                break;
        };
        ?>
    </main>
    
    
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/jquery.tmpl.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/<?=$_GET['page']?>.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    <script src="js/temp.js"></script>
</body>
</html>