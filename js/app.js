$(function(){
    
    $('form').submit(function(){
        url = $(this).attr('action');
        data = $(this).serialize();
        postApi(url, data);
        return false;
    })

    postApi = function (url,dados){
        $.ajax({
            url: 'http://localhost/luisGabriel_yii/web/index.php?r=api/default/get-token',
            method: 'GET',
            dataType: 'json',
            success: function(tk){
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    data: dados+'&_csrf='+tk.token,                    
                    success: function(data){
                        if(data.endPoint.status == 'success'){
                            location.reload();
                        }
                    }
                })
            }
        })
        return ;
       
    }
});