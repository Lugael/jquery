$(function(){
      function renderizaAdm(){
        $('#renderAdm').html('');
        $.template ("carregaAdm", tempAdm);
        $.ajax({
            url: 'http://localhost/luisGabriel_yii/web/index.php?r=api/administradoras/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                if(data.resultSet){
                   $.tmpl("carregaAdm", data.resultSet).appendTo('#renderAdm')
                }
                $('.loader').remove();
            }
        })
    }
    renderizaAdm();
    $(document).on('click', 'a[href="#"].editar', function(){
        idRegistro = $(this).attr('data-id');
        link = $(this);
        $.ajax({
            url: 'http://localhost/luisGabriel_yii/web/index.php?r=api/administradoras/get-one&id='+idRegistro,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                nome = data.resultSet[0].nomeAdm; 
                $(link).parent().parent().find('.nome').html(`<input name="nomeAdm" value="${data.resultSet[0].nomeAdm}">`);
                cnpj = data.resultSet[0].cnpj;
                $(link).parent().parent().find('.cnpj').html(`<input name="cnpj" value="${data.resultSet[0].cnpj}">`);
            }
        })
        //criar botões
        $(link).parent().find('.editar').toggleClass(['editar', 'salvar']);
        $(link).html('Salvar');
        $(link).parent().parent().find('.excluir').html('cancelar');
        $(link).parent().parent().find('.excluir').toggleClass(['excluir', 'cancelar']);
        $('.editar, .excluir').removeAttr('href');
    })
    //cancelar edição
    $(document).on('click', '.cancelar', function(){
       
    })
    
    //editar o registro de fato. Bora negada
    $(document).on('click','.salvar', function(){
        id = $(this).attr('data-id');
        nomeAdm = $('input[name="nomeAdm"]').val();
        cnpj = $('input[name="cnpj"]').val();
        dados = `id=${id}&nomeAdm=${nomeAdm}&cnpj=${cnpj}`;
        if(nomeAdm && cnpj){
            retorno = postApi('http://localhost/luisGabriel_yii/web/index.php?r=api/administradoras/edit-one', dados)
            if(retorno){
                renderizaAdm(); 
            };
        }
    })
    $(document).on('click', '.excluir', function(){
        id = $(this).attr('data-id');
        dados = `id=${id}`;
        postApi('http://localhost/luisGabriel_yii/web/index.php?r=api/administradoras/del',dados);
    })
    
});