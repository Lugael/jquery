$(function(){
    function renderizaUnidade(){
        $('#renderUnid').html('');
        $.template("carregaUnid", tempUnid);
        $.ajax({
            url:'http://localhost/luisGabriel_yii/web/index.php?r=api/unidades/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                if(data.resultSet){
                    $.tmpl("carregaUnid", data.resultSet).appendTo('#renderUnid')
                }
                $('.loader').remove();
            }
        })
    }
    renderizaUnidade();
})